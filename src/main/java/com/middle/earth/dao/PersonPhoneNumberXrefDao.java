package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.PersonPhoneXref;

@Repository
@Transactional
public class PersonPhoneNumberXrefDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save in the database.
	 */
	public void create(PersonPhoneXref ppnXref) {
		entityManager.persist(ppnXref);
		return;
	}
	  
	/**
	 * Delete from the database.
	 */
	public void delete(PersonPhoneXref ppnXref) {
		if (entityManager.contains(ppnXref))
			entityManager.remove(ppnXref);
		else
			entityManager.remove(entityManager.merge(ppnXref));
		return;
	}
	  
	/**
	 * Return all stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<PersonPhoneXref> getAllPersonPhoneNumberXrefs() {
		return entityManager.createQuery("from PersonPhoneNumberXref").getResultList();
	}
	
//	@Autowired
//	SessionFactory sessionFactory;
//	
//	public void save(PersonPhoneNumberXref phoneXref) {		
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.saveOrUpdate(phoneXref);
//	}
//	
//	public void delete(PersonPhoneNumberXref phoneXref) {
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.delete(phoneXref);
//	}

}
