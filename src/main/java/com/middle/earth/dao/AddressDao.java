package com.middle.earth.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.Address;

@Repository
@Transactional
public class AddressDao {
	
	/**
	 * An EntityManager will be automatically injected from entityManagerFactory
	 * setup on DatabaseConfig class.
	 */
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save the user in the database.
	 */
	public void create(Address address) {
		entityManager.persist(address);
		return;
	}
	  
	/**
	 * Delete the user from the database.
	 */
	public void delete(Address address) {
		if (entityManager.contains(address))
			entityManager.remove(address);
		else
			entityManager.remove(entityManager.merge(address));
		return;
	}
	  
	/**
	 * Return all the users stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<Address> getAllAddresses() {
		return entityManager.createQuery("from Address").getResultList();
	}
}
