package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.User;

@Repository
@Transactional
public class UserDao {
	
	@PersistenceContext	(type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	public User getUserById(int userId) {
		return entityManager.find(User.class, userId);
	}
	
	public User getUserByUserName(String userName) {
		String hql = "FROM User user WHERE user.user_name = ?";
		return (User)entityManager.createQuery(hql).setParameter(1, userName).getResultList().get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		String hql = "FROM User";
		List<User> allUsers = entityManager.createQuery(hql).getResultList();
		return allUsers; 
	}
	
	public void addUser(User user) {
		entityManager.persist(user);
	}
	
	public void updateUser(User user) {
		User usercl = getUserById(user.getId());
		usercl.setUserName(user.getUserName());
		usercl.setPassword(user.getPassword());
		entityManager.flush();
	}
	
	public void deleteUser(int userId) {
		entityManager.remove(getUserById(userId));
	}
	
	public boolean userExists(String userName) {
		String hql = "FROM User ucl WHERE ucl.user_name = ?";
		int count = entityManager.createQuery(hql).setParameter(1, userName).getResultList().size();
		return count > 0 ? true : false;
	}
}