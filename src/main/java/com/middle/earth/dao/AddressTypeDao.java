package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.AddressType;

@Repository
@Transactional
public class AddressTypeDao {
	
	/**
	 * An EntityManager will be automatically injected from entityManagerFactory
	 * setup on DatabaseConfig class.
	 */
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save the user in the database.
	 */
	public void create(AddressType addressType) {
		entityManager.persist(addressType);
		return;
	}
	  
	/**
	 * Delete the user from the database.
	 */
	public void delete(AddressType addressType) {
		if (entityManager.contains(addressType))
			entityManager.remove(addressType);
		else
			entityManager.remove(entityManager.merge(addressType));
		return;
	}
	  
	/**
	 * Return all the users stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<AddressType> getAllAddressTypes() {
		return entityManager.createQuery("from AddressType").getResultList();
	}
}
