package com.middle.earth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.middle.earth.hib.State;

@Repository
@Transactional
public class StateDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Save in the database.
	 */
	public void create(State state) {
		entityManager.persist(state);
		return;
	}
	  
	/**
	 * Delete from the database.
	 */
	public void delete(State state) {
		if (entityManager.contains(state))
			entityManager.remove(state);
		else
			entityManager.remove(entityManager.merge(state));
		return;
	}
	  
	/**
	 * Return all stored in the database.
	 */
	@SuppressWarnings("unchecked")
	public List<State> getAllStates() {
		return entityManager.createQuery("from State").getResultList();
	}
	
//	@Autowired
//	SessionFactory sessionFactory;
//	
//	public void save(State state) {		
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.saveOrUpdate(state);
//	}
//	
//	public void delete(State state) {		
//		Session currentSession = sessionFactory.getCurrentSession();
//		currentSession.delete(state);
//	}

}
