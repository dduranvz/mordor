package com.middle.earth.hib;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "persons_addresses_xref")
@IdClass(PersonAddressXref.class)
public class PersonAddressXref extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "person_id")
	Person personId;
	
	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "address_id")
	Address addressId;
	
	@Id
	@NotNull
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "address_type_id")
	AddressType addressTypeId;

	public PersonAddressXref() {
		super();
	}

	public PersonAddressXref(Person personId, Address addressId, AddressType addressTypeId) {
		super();
		this.personId = personId;
		this.addressId = addressId;
		this.addressTypeId = addressTypeId;
	}

	public Person getPersonId() {
		return personId;
	}

	public void setPersonId(Person personId) {
		this.personId = personId;
	}

	public Address getAddressId() {
		return addressId;
	}

	public void setAddressId(Address addressId) {
		this.addressId = addressId;
	}

	public AddressType getAddressTypeId() {
		return addressTypeId;
	}

	public void setAddressTypeId(AddressType addressTypeId) {
		this.addressTypeId = addressTypeId;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.PersonAddressXref getPersonAddressXref() {
		com.middle.earth.jax.PersonAddressXref jaxPersonAddressXref = new com.middle.earth.jax.PersonAddressXref(
				getPersonId().getJaxPerson(), 
				getAddressId().getJaxAddress(), 
				getAddressTypeId().getJaxAddressType()
			);
		return jaxPersonAddressXref;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressId == null) ? 0 : addressId.hashCode());
		result = prime * result + ((addressTypeId == null) ? 0 : addressTypeId.hashCode());
		result = prime * result + ((personId == null) ? 0 : personId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonAddressXref other = (PersonAddressXref) obj;
		if (addressId == null) {
			if (other.addressId != null)
				return false;
		} else if (!addressId.equals(other.addressId))
			return false;
		if (addressTypeId == null) {
			if (other.addressTypeId != null)
				return false;
		} else if (!addressTypeId.equals(other.addressTypeId))
			return false;
		if (personId == null) {
			if (other.personId != null)
				return false;
		} else if (!personId.equals(other.personId))
			return false;
		return true;
	}
}

	