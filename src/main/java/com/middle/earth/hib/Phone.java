package com.middle.earth.hib;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "phones")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"timeCreated", "timeUpdated"})
public class Phone extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_generator")
	@SequenceGenerator(name="phone_generator", sequenceName = "seq_phones")
	@Column(name = "id")
	int id;
	
	@Column(name = "phone_number")
	String phoneNumber;
	
	@Column(name = "time_created", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	Date timeCreated;
	
	@Column(name = "time_updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	Date timeUpdated;

	public Phone() {
		super();
	}
	
	public Phone(int id, String phoneNumber, Date timeCreated, Date timeUpdated) {
		super();
		this.id = id;
		this.phoneNumber = phoneNumber;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	@JsonIgnore
	public com.middle.earth.jax.Phone getJaxPhone() {
		com.middle.earth.jax.Phone jaxPhone = new com.middle.earth.jax.Phone(
				getId(), 
				getPhoneNumber(), 
				getTimeCreated(), 
				getTimeUpdated()
			);
		return jaxPhone;
	}	
}
