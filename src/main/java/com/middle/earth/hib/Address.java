package com.middle.earth.hib;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "addresses")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"timeCreated", "timeUpdated"})
public class Address extends HibBase {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_generator")
	@SequenceGenerator(name="address_generator", sequenceName = "seq_addresses")
	@Column(name = "id")
	int id;	
	
	@Column(name = "street_1")
	String street1;
	
	@Column(name = "street_2")
	String street2;
	
	@Column(name = "city")
	String city;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "state_id")
	State stateId;
	
	@Column(name = "zip")
	String zip;	
	
	@Column(name = "time_created", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	Date timeCreated;	
	
	@Column(name = "time_updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	Date timeUpdated;
			
	public Address() {
		super();
	}

	public Address(int id, String street1, String street2, String city, State stateId, String zip, Date timeCreated,
			Date timeUpdated) {
		super();
		this.id = id;
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.stateId = stateId;
		this.zip = zip;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String address1) {
		this.street1 = address1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String address2) {
		this.street2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public State getStateId() {
		return stateId;
	}

	public void setStateId(State stateId) {
		this.stateId = stateId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	
	@JsonIgnore
	public com.middle.earth.jax.Address getJaxAddress(){
		com.middle.earth.jax.Address jaxAddress = new com.middle.earth.jax.Address(
				getId(),
				getStreet1(),
				getStreet2(),
				getCity(),
				getStateId().getJaxState(),
				getZip(),
				getTimeCreated(),
				getTimeUpdated()
				);		
		return jaxAddress;		
	}
}
