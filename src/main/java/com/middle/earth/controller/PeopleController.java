package com.middle.earth.controller;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.middle.earth.jax.Address;
import com.middle.earth.jax.AddressType;
import com.middle.earth.jax.User;
import com.middle.earth.services.IPeopleService;

@RestController
@Path("/people")
public class PeopleController extends BaseController {
	
	private static Logger log = LogManager.getLogger();	

	@Autowired
	private IPeopleService iPeopleService;

	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String people() {
		return iPeopleService.PeopleMessage();
	}

	@GET
    @Produces({"application/json"})
	@Path("user/{id}")
	public User getUserById(@PathParam("id") Integer id) {
		log.debug("User Id: "+id);
		User user = iPeopleService.getUserById(id);
		return user;
	}
	
    @GET
    @Produces({"application/json"})
    @Path("users")
    public List<User> getAllUsers() {
    	List<User> list = iPeopleService.getAllUsers();
    	try {
    		//	For debugging purposes
    		if(log.isDebugEnabled()) {
    	    	for(User jaxUser : list) {
    	    		log.debug("User Id: "             + jaxUser.getId());
    	    		log.debug("Username: "            + jaxUser.getUserName());
    	    		log.debug("Password: "            + jaxUser.getPassword());
    	    		log.debug("Time Created: "        + jaxUser.getTimeCreated());
    	    		log.debug("Time Updated: "        + jaxUser.getTimeUpdated());
    	    		log.debug("Person's First Name: " + jaxUser.getPerson().getFirstName()); 
    	    		log.debug("Person's Last Name: "  + jaxUser.getPerson().getLastName());
    	    		
    	    		Map<AddressType, Address> address = jaxUser.getPerson().getAddress();
    	    		for(Map.Entry<AddressType, Address> alist : address.entrySet()) {
    	    			log.debug("Street 1: "	+ alist.getValue().getStreet1());
    	    			log.debug("Street 2: "	+ alist.getValue().getStreet2());
    	    			log.debug("City: "      + alist.getValue().getCity());
    	    			log.debug("State: "     + alist.getValue().getStateId().getStateName());
    	    			log.debug("Zip: "       + alist.getValue().getZip());  	    			
    	    		}
    	    	}    	
    		}
    	}
    	catch (NullPointerException e){    		
    		log.error("NullPointerException Caught");    		
    	}
    	return list;    	
    }
 
    
    /*
     * Temporarily commented out, throws exception because of missing Jersey dependency.
     * Working on it.
     */

//    @POST
//    @Consumes({"application/json", "application/x-msgpack"})
//    @Produces({"application/json", "application/x-msgpack"})
//	@Path("user")
//    public ResponseEntity<Void> addUser(@RequestBody User user, UriComponentsBuilder builder) {
//        boolean flag = iPeopleService.addUser(user);
//        if (flag == false) {
//	    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
//        }
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(builder.path("/user/{id}").buildAndExpand(user.getUserId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//    }
    
    @PUT
    @Consumes({"application/json"})
    @Produces({"application/json"})
	@Path("user")
    public User updateUser(@RequestBody User user) {
    	iPeopleService.updateUser(user);
		return user;
	}
    
    @DELETE
    @Produces({"application/json"})
	@Path("user-del/{id}")
    public void deleteUser(@PathParam("id") Integer id) {
    	iPeopleService.deleteUser(id);
	}
    

}
