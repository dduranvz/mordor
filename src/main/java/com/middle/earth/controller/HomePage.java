package com.middle.earth.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.middle.earth.services.HomePageService;

@Controller
@Path("/")
public class HomePage extends BaseController {
	
	@Autowired
	private HomePageService homePage;
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String homepage() {
				
		return homePage.HomeMessage();
	}
}
