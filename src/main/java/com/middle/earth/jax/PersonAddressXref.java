package com.middle.earth.jax;

public class PersonAddressXref extends JaxBase {
	
	private static final long serialVersionUID = 1L;

	Person personId;
	Address addressId;	
	AddressType addressTypeId;
	
	public PersonAddressXref(Person personId, Address addressId, AddressType addressTypeId) {
		super();
		this.personId = personId;
		this.addressId = addressId;
		this.addressTypeId = addressTypeId;
	}

	public Person getPersonId() {
		return personId;
	}

	public void setPersonId(Person personId) {
		this.personId = personId;
	}

	public Address getAddressId() {
		return addressId;
	}

	public void setAddressId(Address addressId) {
		this.addressId = addressId;
	}

	public AddressType getAddressTypeId() {
		return addressTypeId;
	}

	public void setAddressTypeId(AddressType addressTypeId) {
		this.addressTypeId = addressTypeId;
	}
	
}
