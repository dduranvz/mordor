package com.middle.earth.jax;

public class PhoneType extends JaxBase {
	
	private static final long serialVersionUID = 1L;
	
	int id;
	String phoneType;

	public PhoneType(int id, String phoneType) {
		super();
		this.id = id;
		this.phoneType = phoneType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
	
	@Override
    public String toString() {
        return String.format("phoneType: %s", phoneType);
    }
}
