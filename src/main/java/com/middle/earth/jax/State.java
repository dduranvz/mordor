package com.middle.earth.jax;

public class State extends JaxBase {
	
	private static final long serialVersionUID = 1L;

	int id;
	String stateCode;
	String stateName;

	public State(int id, String stateCode, String stateName) {
		super();
		this.id = id;
		this.stateCode = stateCode;
		this.stateName = stateName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}
