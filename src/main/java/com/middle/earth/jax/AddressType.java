package com.middle.earth.jax;

public class AddressType extends JaxBase {
	
	private static final long serialVersionUID = 1L;
	
	int id;
	
	String addressType;

	public AddressType(int id, String addressType) {
		super();
		this.id = id;
		this.addressType = addressType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}		
	
	@Override
    public String toString() {
        return String.format("addressType: %s", addressType);
    }
}
